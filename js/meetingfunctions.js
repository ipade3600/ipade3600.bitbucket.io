var _id = this._id;
var MAX_MEETINGS = 10;

//Names of all automatically set meetings 
var displayingMeetingName = ["Slytherin Dungeon", "Gryffindor Tower", "Hufflepuff Basement", "Ravenclaw Tower", "The Hogwarts Library", "The Arts Classroom", "The Chamber Of Secrets", "The Room Of Requirement", "The Astronomy Room", "The Headmaster Office"];
//var displayingUserName = ["Draco Malfoy", "Harry Potter", "Newt Scamander", "Luna Lovegood", "Irma Pince", "Severus Snape", "Tom Riddle", "UserPreferNotToGiveName", "Aurora Sinistra", "Albus Dumbledore"];
var mainSessionInfo = ["The Hogwarts Great Hall", ""];
var nullUserName = "UserPreferNotToGiveName";
/** General information for the containers and buttosn */
const rawInfo = {
    rawSwitchButtonID: "join-room-",
    rawLeaveButtonID: "leave-room-",
    rawDisplayingMeetingNameID: "screen-name-",
    rawMeetingListContainerID: "mContainer-",
    domain: "stage2.info3600cp28.net"
}

var clickedButtonID = "";



function getScriptContent(_id) {
    var scriptString = document.getElementById(_id).text;
    var scriptArray = scriptString.split(",");
    return scriptArray;
}


/**
 * Switch to meeting given the meeting id
 * @param {String} _id meeting id
 */
function switchToMeeting(_id) {

    var switchToRoomName = "";
    //var switchToUserName = "";
    var switchFromRoomName = mainSessionInfo[0];
    /*var switchFromUserName = mainSessionInfo[1];
    if (!switchFromUserName) {
        switchFromUserName = nullUserName;
    }*/

    //get index of clicked button
    clickedButtonID = _id;
    var index = clickedButtonID.replace(rawInfo.rawSwitchButtonID, "");
    index = parseInt(index);

    //get meeting information for switching to meeting
    switchToRoomName = displayingMeetingName[index];
    /*switchToUserName = displayingUserName[index];
    if (switchToUserName == nullUserName) {
        switchToUserName = null;
    }*/
    mainSessionInfo = [switchToRoomName, nullUserName];

    //change the meeting information in array 
    displayingMeetingName[index] = null;
    //displayingUserName[index] = null;

    renewActiveMeetingList(1);
    //renewUserNameList(1);
    displayingMeetingName[0] = switchFromRoomName;
    renewMeetingNames();
    //displayingUserName[0] = switchFromUserName;

    return mainSessionInfo;
}


/**
 * Upon requesting to leave a meeting, this function is called 
 * @param {String} _id meeting id (button id)
 */
function leaveThisMeeting(_id) {
    clickedButtonID = _id;
    var index = clickedButtonID.replace(rawInfo.rawLeaveButtonID, "");
    displayingMeetingName[index] = null;
    renewActiveMeetingList(0);
    //renewUserNameList(0);
    renewMeetingNames();
    changeContainerVisibility();
}

/**
 * Upon requesting to leave or switch to a meeting, this function is called 
 * @param {int} num indicator of the calling function and corrsponding operation
 * 0 if leaving a meeting, 1  if switching between meetings
 */
function renewActiveMeetingList(num) {
    var tmp_array = new Array(MAX_MEETINGS);
    var ctr = num;
    for (i = 0; i < MAX_MEETINGS; i++) {
        if (displayingMeetingName[i] != null) {
            tmp_array[ctr] = displayingMeetingName[i];
            ctr++;
        }
    }
    displayingMeetingName = tmp_array;
}

/**
 * Upon requesting to leave or switch to a meeting, this function is called 
 * @param {int} num indicator of the calling function and corrsponding operation
 * 0 if leaving a meeting, 1  if switching between meetings
 * temporarily crossed out and have no affection
 */
/* 
function renewUserNameList(num) {
    var tmp_array = new Array(MAX_MEETINGS);
    var ctr = num;
    for (i = 0; i < MAX_MEETINGS; i++) {
        if (displayingUserName[i] != null) {
            tmp_array[ctr] = displayingUserName[i];
            ctr++;
        }
    }
    displayingUserName = tmp_array;
}*/

/**
 * Get number of active meeting rooms 
 */
function getNonEmptyLength() {
    var ctr = 0;
    for (i = 0; i < MAX_MEETINGS; i++) {
        if (displayingMeetingName[i] != null) {
            ctr++;
        }
    }
    return ctr;
}

/**
 * Renew all the original meeting names 
 */
function renewMeetingNames() {
    for (ctr = 0; ctr < MAX_MEETINGS; ctr++) {
        var tmp_screen_name = rawInfo.rawDisplayingMeetingNameID + ctr;
        if (displayingMeetingName[ctr] != null) {
            document.getElementById(tmp_screen_name).innerHTML = displayingMeetingName[ctr];
        } else {
            document.getElementById(tmp_screen_name).innerHTML = "Meeting " + ctr;
        }
    }
}

/** 
 * Change the visibility of the active meeting container depending on how many active meetings are in it
 */
function changeContainerVisibility() {
    var len = getNonEmptyLength();
    var tmp_container = document.getElementsByClassName("individual-active-meeting");
    var tmp_btn_container = document.getElementsByClassName("session-btn-container");
    var tmp_font = document.getElementsByClassName("onscreen-name");
    for (ctr = 0; ctr < MAX_MEETINGS; ctr++) {
        var containerName = rawInfo.rawMeetingListContainerID + ctr;
        if (displayingMeetingName[ctr] != null) {
            if (len <= 5) {
                tmp_container[ctr].style.width = "95%";
                tmp_btn_container[ctr].style.width = "90%";
                tmp_btn_container[ctr].style.marginLeft = "auto";
                tmp_font[ctr].style.fontSize = "108%";
                document.getElementById(containerName).style.display = "block";
            } else {
                tmp_container[ctr].style.width = "42%";
                tmp_btn_container[ctr].style.width = "140%";
                tmp_btn_container[ctr].style.marginLeft = "-46%";
                tmp_font[ctr].style.fontSize = "90%";
                document.getElementById(containerName).style.display = "inline-block";
            }
        } else {
            tmp_container[ctr].style.width = "95%";
            tmp_btn_container[ctr].style.width = "90%";
            tmp_btn_container[ctr].style.marginLeft = "auto";
            tmp_font[ctr].style.fontSize = "108%";
            document.getElementById(containerName).style.display = "none";
        }
    }
}
/**To open and close the join meeting form */
var show_form = document.getElementById('nav-join-form');

show_form.onclick = function showJoinForm() {
    document.getElementById("join-new-form").style.display = "block";
}

function closeForm() {
    document.getElementById("join-new-form").style.display = "none";
}

/**
 * Join a new meeting. Please note, this functionality is not yet in its final form. 
 */
function joinNewMeeting() {
    /* check illegal meeting names */
    var targetMeetingName = document.getElementById("meeting_name").value;
    //var targetUserName = document.getElementById("user_name").value;

    var len = getNonEmptyLength();
    if (targetMeetingName == mainSessionInfo[0]) {
        closeForm();
        return 0;
    }
    /** search displayingMeetingName if already joined*/
    for (i = 0; i < displayingMeetingName.length; i++) {
        if (displayingMeetingName[i] == targetMeetingName) {
            //user trying to join a concurrent meeting
            var toBtn = rawInfo.rawSwitchButtonID + i;
            closeForm();
            var tmpArray = switchToMeeting(toBtn);
            return tmpArray;
        }
    }
    if (len == MAX_MEETINGS) {
        alert("you already have 10 concurrent meetings!");
        return 0;
    }

    displayingMeetingName[len] = targetMeetingName;
    //displayingUserName[len] = targetUserName;
    var toBtn = rawInfo.rawSwitchButtonID + len;
    closeForm();
    var tmpArray = switchToMeeting(toBtn);
    changeContainerVisibility();
    return tmpArray;
}

/**
 * Helper function to display thumbnail
 * @param {*} _id 
 */
function thumbnailDisplay(_id) {
    var thumb = document.getElementById("concurrent_thumbnail");
    if (window.getComputedStyle(thumb).display === "none") {
        thumb.style.display = "block";
    } else {
        thumb.style.display = "none";
    }
}

/**
 * Overwrite original meeting information upon switching meetings
 * @param {1} idx index of room to switch to 
 * @param {String} content name of room to switch to
 * temporarily not using
 */
function overwriteSessionInfo(idx, content) {

    var scriptString = document.getElementById("meetingInfo").text;

    var scriptArray = scriptString.split(",");
    scriptArray[idx] = scriptArray[idx].replace(/".*?"/, '"' + content + '"');
    var returnScriptString = "";
    for (i = 0; i < scriptArray.length; i++) {
        returnScriptString = returnScriptString.concat(scriptArray[i]);
        if (i != scriptArray.length - 1) {
            returnScriptString = returnScriptString.concat(",");
        }
    }
    document.getElementById("meetingInfo").text = returnScriptString;
}